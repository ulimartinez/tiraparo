<?php

class Trabajos_Model extends CI_Model{

  function __construct(){
    parent::__construct();
  }

  function getAllJobs(){
    $query = $this->db->query("select * from trabajos where ePractica = 0");
    return $query->result();
  }

  function getAllPracticas(){
    $query = $this->db->query("select * from trabajos where ePractica = 1");
    return $query->result();
  }

  function getAllPracticasDisponibles(){
    $query = $this->db->query("select * from trabajos where ePractica = 1 and eClosed = 0");
    return $query->result();
  }

  function getAllJobsDisponibles(){
    $query = $this->db->query("select * from trabajos where ePractica = 0 and eClosed = 0");
    return $query->result();
  }

  function insert($eCompany, $ePosition, $eDescription, $ePractica){
    $query = $this->db->query("insert into trabajos(eCompany, ePosition, eDescription, ePractica) values('$eCompany', '$ePosition', '$eDescription', $ePractica)");
    //return $query->result();
  }

}

 ?>
