<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listing extends MY_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index(){
        $this->load->model('trueque/trueque_main_model');
        $this->load->model('user_model');
        $db_results = $this->trueque_main_model->get_all_listings();
        $data = array('listings'=>"");
        foreach($db_results as $result){
            $data1['row'] = $result;
            $data1['user'] = $this->user_model->get_user($result->u_id)[0];
            $data['listings'] .= $this->load->view('trueque/listing_template', $data1, true);
        }
        $this->load->view('trueque/trueque_home', $data);
    }
    public function get_listing($id){
        $this->load->model('listing_model');
        $this->load->model('user_model');
        $data['listing'] = $this->listing_model->getListing($id)[0];
        $data['user'] = $this->user_model->get_user($data['listing']->u_id)[0];
        $this->load->view('trueque/item_view', $data);
    }
    public function filter_category($cat){
        $this->load->model('trueque_main_model');
        $this->load->model('user_model');
        $db_results = $this->trueque_main_model->get_cat_listing($cat);
        $data = array('listings'=>"");
        foreach($db_results as $result){
            $data1['row'] = $result;
            $data1['user'] = $this->user_model->get_user($result->u_id)[0];
            $data['listings'] .= $this->load->view('trueque/listing_template', $data1, true);
        }
        $this->load->view('trueque/trueque_home', $data);
    }
}
?>