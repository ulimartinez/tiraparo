<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aventon extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('aventones/aventones');
	}

	public function filterRequest(){
		$this->load->model('main_model');

		$day = $this->input->post('day');
    $hour1 = $this->input->post('hour1');
    $hour2 = $this->input->post('hour2');
    $to = $this->input->post('to');
		$result = $this->main_model->getFiltererdTrips($day, $hour1, $hour2, $to);
		echo json_encode($result);

	}

	public function insertRequest(){
		$this->load->model('main_model');

		$origin = $this->input->post('crear-origen');
		$destination = $this->input->post('crear-destino');
		$date = $this->input->post('crear-fecha');
		$hour = $this->input->post('crear-hora');
		$seats = $this->input->post('crear-espacios');
		$oGPS = $this->input->post('crear-origen-coordenadas');
		$dGPS = $this->input->post('crear-destino-coordenadas');

		$result = $this->main_model->createTrip($origin, $destination, $date, $hour, $seats, $oGPS, $dGPS);

		$this->index();

	}

}
