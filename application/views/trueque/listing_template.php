<div class="col-lg-4 col-md-6 my-4">
    <div class="card h-100">
        <a href="<?php echo base_url()."listings/get_listing/".$row->p_id; ?>"><img class="card-img-top" src="<?php echo base_url().'/img/'.$row->photo; ?>" alt=""></a>
        <div class="card-body">
            <h4 class="card-title">
                <a href="<?php echo base_url()."listings/get_listing/".$row->p_id; ?>"><?php echo $row->title; ?></a>
            </h4>
            <h5>$<?php echo $row->price; ?>.00</h5>
            <p class="card-text"><?php echo $row->description; ?></p>
            <p class="card-text"><?php echo $user->name; ?></p>
        </div>
        <div class="card-footer">
            <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
        </div>
    </div>
</div>