<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>MiUni | MiTrabajo</title>
    <style>
    .truncate {
      text-overflow: ellipsis;
    }
    .pagination {
      justify-content: center;
    }
    </style>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>css/v2/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>css/logo-nav.css" rel="stylesheet">
  </head>
  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#"> MiUni
          <!--<img src="http://placehold.it/300x60?text=Logo" width="150" height="30" alt="">-->
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Bolsa de Trabajo
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Bazar</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Aventones</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">
      <div class="float-right"><br>
          <span>Tu perfil &nbsp;</span><img id="user_img" class="img-responsive" style="width:50px;height:50px;" src="https://openclipart.org/download/247319/abstract-user-flat-3.svg"></img>
      </div><br>
      <h1 class="mt-5">MiTrabajo</h1>
      <p>Encuentra culquier tipo de practicas profesionales o trabajos para estudiantes.</p>
      <div class="row">
        <div class="col">
          <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" href="#home" role="tab">Practicas</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#profile" role="tab">Trabajos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#agregar" role="tab">Agregar</a>
            </li>
          </ul>
          <div class="container"><br>
          </div>

          <div class="tab-content">
            <div class="tab-pane active" id="home" role="tabpanel">
              <div class="container">
                <p id="cuantas_trabajos">Hay <?php echo sizeof($practicas);?> practicas. <?php echo sizeof($practicas_disponibles);?> de ellas aun estan disponibles.</p>
                <div class="row">
                  <?php
                  for ($i=0; $i < sizeof($practicas); $i++) {
                    $eCompany = $practicas[$i]->eCompany;
                    $ePosition = $practicas[$i]->ePosition;
                    $eDescription = $practicas[$i]->eDescription;
                    $eClosed = $practicas[$i]->eClosed;
                    if($eClosed == 1){
                      $eClosed = "<strong>Aplicacion ha vencido. </strong> <br>";
                      $button = "<a href=\"#\" class=\"btn btn-default disabled \">Aplicar</a>";
                    }else{
                      $eClosed = "Aplicacion sigue <strong>vigente</strong>. <br>";
                      $button = "<a href=\"#\" class=\"aplicado btn btn-primary\">Aplicar</a>";
                    }

                    $eDate = $practicas[$i]->eDate;
                    echo "<div class=\"col-sm-12 col-md-6\">
                    <div class=\"card\">
                    <div class=\"card-header text-center\"><strong>
                    $eCompany - $ePosition</strong>
                    </div>
                    <div class=\"card-content\">
                    <div class=\"container\">
                    <div class=\"row\">
                    <div class=\"col text-center truncate\"><br>
                    $eDescription
                    </div>
                    </div>
                    </div>
                    </div>
                    <div class=\"card-block text-center\"><hr> $button
                    <div class=\"row\"> <br></div>
                    </div>
                    <div class=\"card-footer text-muted\">
                    $eClosed Post hecho el $eDate.
                    </div>
                    </div><br></div>";
                  }
                  ?>
                  <div class="container">
                    <div class="row">
                      <div class="col">
                        <ul class="pagination">
                          <li class="page-item"><a class="page-link" >Previous</a></li>
                          <li class="page-item active"><a class="page-link" >1</a></li>
                          <li class="page-item"><a class="page-link" >2</a></li>
                          <li class="page-item"><a class="page-link" >3</a></li>
                          <li class="page-item"><a class="page-link" >Next</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="tab-pane" id="profile" role="tabpanel">
              <div class="container">
                <p id="cuantas_trabajos">Hay <?php echo sizeof($trabajos);?> trabajos. <?php echo sizeof($trabajos_disponibles);?> de ellos aun estan disponibles.</p>
                <div class="row">
                  <?php
                  for ($i=0; $i < sizeof($trabajos); $i++) {
                    $eCompany = $trabajos[$i]->eCompany;
                    $ePosition = $trabajos[$i]->ePosition;
                    $eDescription = $trabajos[$i]->eDescription;
                    $eClosed = $trabajos[$i]->eClosed;
                    if($eClosed == 1){
                      $eClosed = "<strong>Aplicacion ha vencido. </strong> <br>";
                      $button = "<a href=\"#\" class=\"btn btn-default disabled \">Aplicar</a>";
                    }else{
                      $eClosed = "Aplicacion sigue <strong>vigente</strong>. <br>";
                      $button = "<a href=\"#\" class=\"aplicado btn btn-primary\">Aplicar</a>";
                    }

                    $eDate = $trabajos[$i]->eDate;
                    echo "<div class=\"col-sm-12 col-md-6\">
                    <div class=\"card\">
                    <div class=\"card-header text-center\"><strong>
                    $eCompany - $ePosition</strong>
                    </div>
                    <div class=\"card-content\">
                    <div class=\"container\">
                    <div class=\"row\">
                    <div class=\"col text-center truncate\"><br>
                    $eDescription
                    </div>
                    </div>
                    </div>
                    </div>
                    <div class=\"card-block text-center\"><hr> $button
                    <div class=\"row\"> <br></div>
                    </div>
                    <div class=\"card-footer text-muted\">
                    $eClosed Post hecho el $eDate.
                    </div>
                    </div><br></div>";
                  }
                  ?>
                  <div class="container">
                    <div class="row">
                      <div class="col">
                        <ul class="pagination">
                          <li class="page-item"><a class="page-link" >Previous</a></li>
                          <li class="page-item active"><a class="page-link" >1</a></li>
                          <li class="page-item"><a class="page-link" >2</a></li>
                          <li class="page-item"><a class="page-link" >3</a></li>
                          <li class="page-item"><a class="page-link" >Next</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="tab-pane" id="agregar" role="tabpanel">
              <div class="container">
                <div class="row">
                  <form>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Contratista</label>
                      <input type="email" class="form-control" id="contratista" aria-describedby="emailHelp" placeholder="Anota el nombre del contratista">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Puesto</label>
                      <input type="email" class="form-control" id="puesto" placeholder="Menciona el puesto">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Description</label>
                      <input type="email" class="form-control" id="description" placeholder="Una breve descripcion del puesto">
                    </div>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input id="practica" type="checkbox" class="form-check-input">
                        Practica (no marcar significa empleo)
                      </label>
                    </div>
                    <button id="button_aplicado" type="submit" class="btn btn-primary" onclick="subir">Subir</button>
                  </form>
                </div>
              </div>
            </div>

          </div>



            </div>
          </div>
        </div>

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">David Martinez</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                Escuela: IADA <br> Carrera: Licenciatura en Arquitectura <br> Promedio: 9.47 <br> Cursando 6to semestre<hr>
                <h3 class="text-center">Logros:</h3> <br>
                <strong>Recomendado por <em> Bosch </em></strong><br>
                <strong>Recomendado por <em> Genpact </em></strong><br>
                <!--<strong>Conductor de 4.57 estrellas, determinado por evaluaciones de <em> Aventones</em></strong><br>
                <strong>Vendedor evaluado en 4.89 estrellas, por el <em>Bazar</em></strong><br>-->
                <br>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Configurar Curriculum</button>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="modalAplicado" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">

            <div class="modal-body">
              <h3 class="text-center"> ¡Gracias por aplicar! </h3><br>
              <h4 class="center-text"> El contratista estará en contacto contigo entre los proximos dias. <br> Ponte al pendiente.</h4>
              <br>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal">OK!</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.container -->

    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo base_url();?>jquery/jquery.min.js"></script>
    <script src="<?php echo base_url();?>js/v2/bootstrap.bundle.min.js"></script>
    <script>
    $(document).ready(function() {
      $('#myTab nuevo-tab, popular-tab').on('click', function (e) {
        e.preventDefault()
        $(this).tab('show')
      });

      $("#user_img").click(function() {
        mostrarUsuario();
      });

      $(".aplicado").click(function(){
        aplicado();
      });

      $("#button_aplicado").click(function(){
        subir();
      });

      function mostrarUsuario(){
        $('#myModal').modal('show');
      }

      function aplicado(){
        $('#modalAplicado').modal('show');
      }
    });

    function subir(){
      var contratista = $("#contratista").val();
      var puesto = $("#puesto").val();
      var description = $("#description").val();
      var practica = $("#practica");
      if(practica[0].checked){
        practica = 1;
      }else{
        practica = 0;
      }

      var query = {
        eCompany:contratista,
        ePosition:puesto,
        eDescription:description,
        ePractica:practica
      };

      $.post('<?php echo base_url();?>trabajos/insert',query,function(data){

      }).done(function(data){

      });
      location.reload();
    }
    </script>
  </body>
</html>
